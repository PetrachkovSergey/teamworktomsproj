﻿using System;
using System.Linq;

namespace TeamworkMsprojectExportTweaker
{
    /// <summary>
    /// ViewModel for window.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Creates new MainViewModel.
        /// </summary>
        public MainViewModel()
        {
            LoginVm = new LoginViewModel();
            FixerVm = new FixerViewModel();
        }
        /// <summary>
        /// Gets or sets login area ViewModel.
        /// </summary>
        public LoginViewModel LoginVm { get; set; }
        /// <summary>
        /// Gets or sets fixer area ViewModel.
        /// </summary>
        public FixerViewModel FixerVm { get; set; }
    }
}