﻿using System;
using System.Linq;
using System.Net;
using NLog;
using TeamworkMsprojectExportTweaker.Properties;

namespace TeamworkMsprojectExportTweaker
{
    /// <summary>
    /// ViewModel for login area.
    /// </summary>
    public class LoginViewModel : ViewModelBase
    {
        static readonly ILogger logger = LogManager.GetCurrentClassLogger();

        public LoginViewModel()
        {
            ApiKey = Settings.Default.ApiKey;
            ApiUrl = Settings.Default.ApiUrl;
            Slow = Settings.Default.Slow;
            LoginText = "Log in";
        }

        private string apikey;

        /// <summary>
        /// Gets or sets TeamWork api key.
        /// </summary>
        public string ApiKey
        {
            get { return apikey; }
            set
            {
                apikey = value;
                OnPropertyChanged(() => ApiKey);
                WrongApiKey = false;
            }
        }

        private string apiurl;
        /// <summary>
        /// Gets or sets url of TeamWork server (optional, we may get it doing additional API call).
        /// </summary>
        public string ApiUrl
        {
            get { return apiurl; }
            set
            {
                apiurl = value;
                OnPropertyChanged(() => ApiUrl);
            }
        }

        private bool logged;
        /// <summary>
        /// Gets or sets a value indicating whether user logged in.
        /// </summary>
        public bool Logged
        {
            get { return logged; }
            set
            {
                logged = value;
                OnPropertyChanged(() => Logged);
                OnPropertyChanged(() => NotLogged);
            }
        }
        /// <summary>
        /// Gets or sets a value indicating whether user not logged in, for simple binding.
        /// </summary>
        public bool NotLogged
        {
            get { return !logged; }
        }

        private bool wrongApiKey;
        /// <summary>
        /// Gets or sets a value indicating whether user trying to log in with wrong api key.
        /// </summary>
        public bool WrongApiKey
        {
            get { return wrongApiKey; }
            set
            {
                wrongApiKey = value;
                OnPropertyChanged(() => WrongApiKey);
            }
        }

        private bool slow;
        /// <summary>
        /// Gets or sets a value indicating whether user treats his network connection as slow.
        /// </summary>
        public bool Slow
        {
            get { return slow; }
            set
            {
                slow = value;
                OnPropertyChanged(() => Slow);
            }
        }

        private string loginText;
        /// <summary>
        /// Gets or sets content of Login button.
        /// </summary>
        public string LoginText
        {
            get { return loginText; }
            set
            {
                loginText = value;
                OnPropertyChanged(() => LoginText);
            }
        }
        /// <summary>
        /// Gets command for Login button.
        /// </summary>
        public RelayCommand LoginCommand
        {
            get
            {
                return new RelayCommand(() =>
                {
                    var client = TeamWorkClient.Make(ApiKey, Slow, ApiUrl);

                    logger.Info("logging with apikey '{0}**', url '{1}', slow {2}", ApiKey[0], ApiUrl, Slow);

                    TryLogIn(client);

                    Settings.Default.ApiKey = ApiKey;
                    Settings.Default.ApiUrl = ApiUrl;
                    Settings.Default.Slow = Slow;
                    Settings.Default.Save();
                },
                () => !string.IsNullOrEmpty(ApiKey));
            }
        }

        private void TryLogIn(TeamWorkClient client)
        {
            // countdown logging attempts if ApiUrl if null
            for (int i = 10 - 1; i >= 0; i--)
            {
                try
                {
                    Logged = client.TryLogin();
                    break;
                }
                catch (WebException ex)
                {
                    logger.Error(ex);
                    var webRes = ex.Response as HttpWebResponse;
                    if (webRes != null)
                    {
                        if (webRes.StatusCode == HttpStatusCode.Unauthorized)
                        {
                            WrongApiKey = true;
                            break;
                        }
                    }
                    Logged = false;
                }

                logger.Info("logging: wrong = {0}, attempt = {1}", WrongApiKey, i);
                LoginText = string.Format("Log in ({0})", i);
            }
        }
    }
}