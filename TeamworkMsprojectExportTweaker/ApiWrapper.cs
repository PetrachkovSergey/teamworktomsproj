﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using NLog;

namespace TeamworkMsprojectExportTweaker
{
    /// <summary>
    /// Provides methods for downloading TeamWork data from some TeamWork account.
    /// </summary>
    internal class ApiWrapper
    {
        static readonly ILogger logger = LogManager.GetCurrentClassLogger();
        int maxTrys = 3;
        TeamWorkClient client;
        NetworkCredential cred;
        List<TwProject> _projectsList;
        int secsPerTry;

        /// <summary>
        /// Creates new ApiWrapper for given TeamWorkClient. Client for slow connetion gets bigger request timeout.
        /// </summary>
        public ApiWrapper(TeamWorkClient client)
        {
            this.client = client;
            this.secsPerTry = (client.Slow ? 60 : 15);
            this.cred = new NetworkCredential(client.ApiKey, "X");
        }

        /// <summary>
        /// Gets list of all TeamWork projects.
        /// </summary>
        public List<TwProject> ProjectsList
        {
            get
            {
                return _projectsList ?? (_projectsList = FetchProjectsList());
            }
        }

        /// <summary>
        /// Gets list of all TeamWork projects.
        /// </summary>
        /// <returns></returns>
        public List<TwProject> FetchProjectsList()
        {
            logger.Debug("in {0}", "FetchProjectsList");
            return FetchWithRetry(FetchProjectsListInner, -1);
        }

        /// <summary>
        /// Gets TeamWork tasks of project with specified id.
        /// </summary>
        public List<TwItem> FetchProjectTasks(int id)
        {
            logger.Debug("in {0} {1}", "FetchProjectTasks", id);
            return FetchWithRetry(FetchProjectTasksInner, id);
        }

        /// <summary>
        /// Gets TeamWork task lists of project with specified id.
        /// </summary>
        public List<TwItemList> FetchProjectTaskLists(int id)
        {
            logger.Debug("in {0} {1}", "FetchProjectTaskLists", id);
            return FetchWithRetry(FetchProjectTaskListsInner, id);
        }

        /// <summary>
        /// Gets TeamWork tasks of task list with specified id.
        /// </summary>
        public List<TwItem> FetchTaskListTasks(int id)
        {
            logger.Debug("in {0} {1}", "FetchTaskListTasks", id);
            return FetchWithRetry(FetchTaskListTasksInner, id);
        }

        /// <summary>
        /// Gets list of all projects using specified WebClient.
        /// </summary>
        private List<TwProject> FetchProjectsListInner(WebClient wc, int id = 0)
        {
            var xml =
#if DEBUG
 File.ReadAllText(@"projects.xml");
#else
 wc.DownloadString(GetProjectsUrl());
#endif

            return xml.ParseProjects();
        }

        /// <summary>
        /// Gets list of tasks of project with specified id using specified WebClient.
        /// </summary>
        private List<TwItem> FetchProjectTasksInner(WebClient wc, int id)
        {
            var xml =
#if DEBUG
 File.ReadAllText(@"tasks.xml");
#else
 wc.DownloadString(GetTasksOfProjectUrl(id));
#endif
            return xml.ParseItems();
        }

        /// <summary>
        /// Gets list of task lists of project with specified id using specified WebClient.
        /// </summary>
        private List<TwItemList> FetchProjectTaskListsInner(WebClient wc, int id)
        {
            var xml = wc.DownloadString(GetTaskListsOfProject(id));
            return xml.ParseLists();
        }

        /// <summary>
        /// Gets list of tasks of task list with specified id using specified WebClient.
        /// </summary>
        private List<TwItem> FetchTaskListTasksInner(WebClient wc, int id)
        {
            var xml = wc.DownloadString(GetTasksOfTaskListUrl(id));
            return xml.ParseItems();
        }

        /// <summary>
        /// Wrapper for some fetcher function with retrying policy.
        /// </summary>
        private List<T> FetchWithRetry<T>(Func<WebClient, int, List<T>> f, int id)
        {
            for (int i = 1; i <= maxTrys; i++)
            {
                using (var wc = new MyWebClient(MsToWait(i)))
                {
                    wc.Credentials = cred;
                    try
                    {
                        return f(wc, id);
                    }
                    catch (WebException ex)
                    {
                        logger.Error(ex);
                    }
                }
            }
            throw new RetryLimitException(maxTrys.ToString());
        }

        /// <summary>
        /// Computes WebRequest Timeout for given attempt.
        /// </summary>
        private int MsToWait(int attempt)
        {
            return attempt * secsPerTry * 1000;
        }

        /// <summary>
        /// Returns url for xml of all projects.
        /// </summary>
        private string GetProjectsUrl()
        {
            return string.Format("{0}projects.xml", client.BaseUrl);
        }

        /// <summary>
        /// Returns url for xml of tasks of project with specified id.
        /// </summary>
        private string GetTasksOfProjectUrl(int projectId)
        {
            return string.Format("{0}projects/{1}/tasks.xml", client.BaseUrl, projectId);
        }

        /// <summary>
        /// Returns url for xml of tasks of tasklist with specified id.
        /// </summary>
        private string GetTasksOfTaskListUrl(int taskListId)
        {
            return string.Format("{0}tasklists/{1}/tasks.xml", client.BaseUrl, taskListId);
        }

        /// <summary>
        /// Returns url for xml of task lists of project with specified id.
        /// </summary>
        private string GetTaskListsOfProject(int projectId)
        {
            return string.Format("{0}projects/{1}/tasklists.xml", client.BaseUrl, projectId);
        }

        /// <summary>
        /// WebClient with flexible WebRequest.Timeout.
        /// </summary>
        private class MyWebClient : WebClient
        {
            static readonly ILogger logger = LogManager.GetCurrentClassLogger();
            int timeout;

            /// <summary>
            /// Creates new MyWebClient setting WebRequest.Timeout to given nubmer of miliseconds.
            /// </summary>
            public MyWebClient(int ms)
            {
                timeout = ms;
            }

            /// <summary>
            /// Returns a System.Net.WebRequest object for the specified resource, with tuned Timeout.
            /// </summary>
            protected override WebRequest GetWebRequest(Uri uri)
            {
                WebRequest w = base.GetWebRequest(uri);
                IWebProxy proxy = w.Proxy;
                if (proxy != null)
                {
                    logger.Debug("Proxy: {0}", proxy.GetProxy(w.RequestUri));
                }

                w.Timeout = timeout;
                return w;
            }
        }
    }
}