﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TeamworkMsprojectExportTweaker
{
    /// <summary>
    /// Provides the base class from which the classes that represent TeamWork API entities are derived.
    /// </summary>
    public abstract class TwBase
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1}", Id, Name);
        }
    }

    /// <summary>
    /// Represents TeamWork project.
    ///
    /// see http://developer.teamwork.com/projectsapi
    /// </summary>
    public class TwProject : TwBase
    {
        public List<TwItemList> Lists { get; set; }
    }

    /// <summary>
    /// Represents TeamWork task list.
    ///
    /// see http://developer.teamwork.com/tasklists
    /// </summary>
    public class TwItemList : TwBase
    {
        public List<TwItem> Items { get; set; }
    }

    /// <summary>
    /// Represents TeamWork task.
    ///
    /// see http://developer.teamwork.com/todolistitems
    /// </summary>
    public class TwItem : TwBase
    {
        public int? ParentId { get; set; }
        public int Progress { get; set; }
        public int Order { get; set; }
        public List<TwItem> Childs { get; set; }
    }
}