﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using NLog;

namespace TeamworkMsprojectExportTweaker
{
    /// <summary>
    /// Provides method to fix Ms Project model with broken Percent Complete.
    /// </summary>
    public class Fixer
    {
        static readonly XNamespace ns = "http://schemas.microsoft.com/project";
        static readonly ILogger logger = LogManager.GetCurrentClassLogger();

        Project msProject;

        /// <summary>
        /// Creates Fixer for given Ms Project model.
        /// </summary>
        public Fixer(Project badMsProject)
        {
            this.msProject = badMsProject;

            // ms project hangs when open "Change working time" with 0 resource
            var badRes = msProject.Xml.Descendants(ns + "Resource").Where(x => x.Element(ns + "UID").Value == "0").FirstOrDefault();
            badRes.Remove();
        }

        /// <summary>
        /// Returns XML document for Ms Project fixed by given TeamWork project model.
        /// 
        /// Tries to match Ms Project Task with TeamWork task lists and tasks by name. 
        /// If matched Task is single in list, fixes that Task, else skips.
        /// Tasks with same names in different task lists processed.
        /// </summary>
        public XDocument Fix(TwProject twProject)
        {
            logger.Info("start fixing '{0}' project", twProject.Name);

            var tasksPerList = twProject.Lists.Select(x =>
                new TasksOfItemList()
                {
                    List = x,
                    Tasks = msProject.Tasks
                        .Where(z => z.Assignments.Count == 0)
                        .Where(z => z.Name == x.Name).ToList()
                })
                .ToList();

            foreach (var tsPL in tasksPerList)
            {
                var listname = tsPL.List.Name;
                if (tsPL.Tasks.Count == 1)
                {
                    var listTask = tsPL.Tasks[0];
                    logger.Info("= fixing list {0}", listname);

                    var nextListTaskId = GetNextListTaskId(tasksPerList, tsPL);

                    var tasksPerItem = tsPL.List.Items.Select(x =>
                        new TasksOfItem()
                        {
                            Item = x,

                            // may be same name of items, but in diff lists
                            // ms tasks follows grouping task, id by id
                            // hope there is no samenamed in each list
                            Tasks = msProject.Tasks
                                .Where(z => listTask.ID < z.ID && z.ID < nextListTaskId)
                                .Where(z => z.Name == x.Name).ToList()
                        })
                        .ToList();

                    foreach (var tsPI in tasksPerItem)
                    {
                        var itemName = tsPI.Item.Name;

                        if (tsPI.Tasks.Count == 1)
                        {
                            logger.Info("== fixing item {0} - {1}%", itemName, tsPI.Item.Progress);
                            FixItem(tsPI.Tasks[0], tsPI.Item.Progress);
                        }
                        else if (tsPI.Tasks.Count == 0)
                        {
                            logger.Debug("== no tasks for item {0}", itemName);
                        }
                        else
                        {
                            logger.Warn("== many tasks for item {0} - {1}", itemName, tsPI.Tasks.Count);
                        }
                    }
                }
                else if (tsPL.Tasks.Count == 0)
                {
                    logger.Debug("= no tasks for list {0}", listname);
                }
                else
                {
                    logger.Warn("= many tasks for list {0} - {1}", listname, tsPL.Tasks.Count);
                }
            }

            return new XDocument(msProject.Xml);
        }

        /// <summary>
        /// Get id of Ms Project summary Task reprsenting Teamwork task list, next to specified TasksOfItemList.
        /// 
        /// Retruns int.MaxValue if task list was last in list or there is more than one matched Ms Project Task for task list.
        /// </summary>
        private int GetNextListTaskId(List<TasksOfItemList> tasksPerList, TasksOfItemList current)
        {
            var nextInd = tasksPerList.IndexOf(current) + 1;
            if (nextInd < tasksPerList.Count)
            {
                if (tasksPerList[nextInd].Tasks.Count == 1)
                {
                    logger.Debug("next list task id = {0}", tasksPerList[nextInd].Tasks[0].ID);
                    return tasksPerList[nextInd].Tasks[0].ID;
                }
                else if (tasksPerList[nextInd].Tasks.Count == 0)
                {
                    // Teawork lists without tasks not exported to XML
                    // no task in xml for that list, skip
                    return GetNextListTaskId(tasksPerList, tasksPerList[nextInd]);
                }
                else
                {
                    logger.Debug("not single task for list {0}", tasksPerList[nextInd].List.Name);
                    return int.MaxValue;
                }
            }
            else
            {
                return int.MaxValue;
            }
        }

        /// <summary>
        /// Modifies Ms Project Task, so that Percent Complete field, computed by Ms Project, shown as specified percent.
        ///
        /// Percents in xml ignored and calculated by ms project: PercentComplete = ActualDuration/Duration and PercentWorkComplete = ActualWork/Work.
        /// So we add missing ActualDuration, ActualStart xml nodes.
        ///
        /// Ms Project distinguishes time for task and time for work.
        /// Every man, working on Task (worker) has his own Assignment.
        /// Task Work time = sum of Work time for each of Task's Assigments.
        /// So we add missing and update some of xml nodes for each Assigment.
        ///
        /// If there was not any person for task in Teamwork, assignments for task is empty, we add ActualWork node for Task itself.
        /// </summary>
        private static void FixItem(Task itemTask, int percent)
        {
            // start/finish in xml is Teamwork export date
            var manualStart = itemTask.ManualStart;
            var manualFinish = itemTask.ManualFinish;

            var workTime = TimeParser.ToTime(itemTask.Work);
            var durTime = TimeParser.ToTime(itemTask.Duration);
            if (workTime == TimeSpan.Zero)
            {
                logger.Debug("no work for task {0}", itemTask);
            }
            if (durTime == TimeSpan.Zero)
            {
                logger.Debug("no duration for task {0}", itemTask);
            }

            var actualDurationString = PercentsOfTime(durTime, percent);
            var remainDurationString = PercentsOfTime(durTime, 100 - percent);
            //logger.Trace("{0}% of {1} = {2}", percent, work, actualDurationString);

            //itemTask.Xml.Add(new XElement(ns + "PercentWorkComplete", percent));
            //itemTask.Xml.Add(new XElement(ns + "PercentComplete", percent));

            itemTask.Xml.Add(new XElement(ns + "ActualDuration", actualDurationString));
            itemTask.Xml.Add(new XElement(ns + "RemainingDuration", remainDurationString));

            itemTask.Xml.Add(new XElement(ns + "ActualStart", manualStart));
            itemTask.Xml.Element(ns + "Start").Value = manualStart;
            itemTask.Xml.Element(ns + "Finish").Value = manualFinish;

            var assCount = itemTask.Assignments.Count;
            var workersCount = assCount;
            if (assCount == 0)
            {
                logger.Debug("no assigments for {0}", itemTask.Name);
                // if no assignments, imagine that there is single worker
                workersCount = 1;
            }

            // everybody start and progress together, with same work
            // so total work in task = sum of work for every worker
            var totalWorkStr = PercentsOfTime(workTime, 100 * workersCount);
            var totalActWorkStr = PercentsOfTime(workTime, percent * workersCount);
            var totalRemWorkStr = PercentsOfTime(workTime, (100 - percent) * workersCount);

            // total work in xml incremented if workers > 1
            itemTask.Xml.Element(ns + "Work").Value = totalWorkStr;

            if (assCount == 0)
            {
                // else work is summed from assigments
                itemTask.Xml.Add(new XElement(ns + "ActualWork", totalActWorkStr));
                itemTask.Xml.Add(new XElement(ns + "RemainingWork", totalRemWorkStr));
            }

            // use to tweak WorkCompletedPercent if need
            var perAssMulti = 1;
            var workPerAssString = PercentsOfTime(workTime, 100 * perAssMulti);
            var actWorkPerAssString = PercentsOfTime(workTime, percent * perAssMulti);
            var remWorkPerAssString = PercentsOfTime(workTime, (100 - percent) * perAssMulti);
            if (assCount > 1)
            {
                logger.Debug("{0} assignments, each has {1} work", assCount, workPerAssString);
            }
            foreach (var ass in itemTask.Assignments)
            {
                ass.Xml.Add(new XElement(ns + "ActualStart", manualStart));
                ass.Xml.Add(new XElement(ns + "PercentWorkComplete", percent));

                ass.Xml.Element(ns + "ActualWork").Value = actWorkPerAssString;
                ass.Xml.Element(ns + "RemainingWork").Value = remWorkPerAssString;
                ass.Xml.Element(ns + "Work").Value = workPerAssString;
                ass.Xml.Element(ns + "RegularWork").Value = workPerAssString;
            }
        }

        /// <summary>
        /// Gets Ms Project time string for percentage of specified time span.
        /// </summary>
        private static string PercentsOfTime(TimeSpan time, int percent)
        {
            var t = new TimeSpan(time.Ticks * percent / 100);
            return TimeParser.ToString(t);
        }

        /// <summary>
        /// Represents Ms Project Task models matching TeamWork tasklist.
        /// </summary>
        private class TasksOfItemList
        {
            /// <summary>
            /// Gets or sets TeamWork task list.
            /// </summary>
            public TwItemList List { get; set; }

            /// <summary>
            /// Gets or sets candidates to match List.
            /// </summary>
            public IList<Task> Tasks { get; set; }

            public override string ToString()
            {
                return string.Format("{0} : {1}", List, Tasks.Count == 1 ? Tasks[0].ToString() : Tasks.Count.ToString());
            }
        }

        /// <summary>
        /// Represents Ms Project Task models matching TeamWork task.
        /// </summary>
        private class TasksOfItem
        {
            /// <summary>
            /// Gets or sets TeamWork task.
            /// </summary>
            public TwItem Item { get; set; }

            /// <summary>
            /// Gets or sets candidates to match Item.
            /// </summary>
            public IList<Task> Tasks { get; set; }

            public override string ToString()
            {
                return string.Format("{0} : {1}", Item, Tasks.Count == 1 ? Tasks[0].ToString() : Tasks.Count.ToString());
            }
        }
    }
}