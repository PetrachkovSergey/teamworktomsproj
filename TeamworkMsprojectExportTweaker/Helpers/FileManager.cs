﻿using System;
using System.IO;
using System.Linq;

namespace TeamworkMsprojectExportTweaker
{
    /// <summary>
    /// Helper class for working with file system.
    /// 
    /// MAYBE add ability to select destination folder.
    /// </summary>
    internal class FileManager
    {
        /// <summary>
        /// Returns filename for resulting file for passed original file.
        /// 
        /// Saves files to the same folder, adding suffix.
        /// If file with such name already exists, adds number counter to filename.
        /// </summary>
        public static string GetResultFilename(string origFile)
        {
            var newName = Path.Combine(Path.GetDirectoryName(origFile), Path.GetFileNameWithoutExtension(origFile) + "_fixed");
            var newFileName = newName + Path.GetExtension(origFile);
            var counter = 2;
            while (File.Exists(newFileName))
            {
                newFileName = newName + counter++ + Path.GetExtension(origFile);
            }

            return newFileName;
        }
    }
}