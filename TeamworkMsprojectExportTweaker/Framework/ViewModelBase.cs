﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Windows;

namespace TeamworkMsprojectExportTweaker
{
    /// <summary>
    /// Abstract base class for a ViewModel implementation, implements INotifyPropertyChanged.
    /// </summary>
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        protected readonly static TaskFactory uiTaskFactory;
        private static bool? _isInDesignMode;

        static ViewModelBase()
        {
            if (IsInDesignMode)
            {
                // we may load dlls here
            }
            else
            {
                var uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();
                uiTaskFactory = new TaskFactory(uiScheduler);
            }
        }

        /// <summary>
        /// Gets a value indicating whether we are in design mode.
        /// </summary>
        public static bool IsInDesignMode
        {
            get
            {
                if (!_isInDesignMode.HasValue)
                {
                    _isInDesignMode = (bool)DesignerProperties.IsInDesignModeProperty.GetMetadata(typeof(DependencyObject)).DefaultValue;
                }

                return _isInDesignMode.GetValueOrDefault();
            }
        }

        #region INotifyPropertyChanged Members

        public virtual event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Invokes PropertyChanged event for passed properties. Pass null to raise for every property.
        /// </summary>
        [DebuggerStepThrough]
        protected void OnPropertyChanged(params string[] propertyNames)
        {
            foreach (string name in propertyNames)
            {
                PropertyChangedEventHandler handler = this.PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs(name));
                }
            }
        }

        /// <summary>
        /// Invokes PropertyChanged event for property.
        /// </summary>
        [DebuggerStepThrough]
        protected void OnPropertyChanged<T>(Expression<Func<T>> propertyExpression)
        {
            var memberExpr = propertyExpression.Body as MemberExpression;
            if (memberExpr == null)
            {
                throw new ArgumentException("The expression is not a member access expression.", "propertyExpression");
            }
            string memberName = memberExpr.Member.Name;
            OnPropertyChanged(memberName);
        }

        #endregion INotifyPropertyChanged Members
    }
}