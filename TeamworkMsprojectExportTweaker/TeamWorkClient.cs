﻿using System;
using System.Linq;
using System.Net;
using System.Xml.Linq;
using NLog;

namespace TeamworkMsprojectExportTweaker
{
    /// <summary>
    /// Provides TeamWork API connection related data.
    /// </summary>
    public class TeamWorkClient
    {
        static readonly ILogger logger = LogManager.GetCurrentClassLogger();
        static TeamWorkClient instance;

        private TeamWorkClient()
        {
        }

        /// <summary>
        /// Occurs when new instanse of TeamWorkClient made.
        /// </summary>
        public static event EventHandler Created;

        /// <summary>
        /// Gets current instance of TeamWorkClient.
        /// </summary>
        public static TeamWorkClient Current { get { return instance; } }

        /// <summary>
        /// Gets url of TeamWork server to connect.
        /// </summary>
        public string BaseUrl { get; private set; }

        /// <summary>
        /// Gets TeamWork api key.
        /// </summary>
        public string ApiKey { get; private set; }

        /// <summary>
        /// Gets a value indicating whether user treats his network connection as slow.
        /// </summary>
        public bool Slow { get; private set; }

        /// <summary>
        /// Creates new TeamWorkClient instance.
        /// </summary>
        /// <returns></returns>
        public static TeamWorkClient Make(string apiKey, bool slow, string baseUrl = null)
        {
            instance = new TeamWorkClient();
            if (!string.IsNullOrEmpty(baseUrl))
                instance.BaseUrl = new UriBuilder(baseUrl).Uri.ToString();
            instance.ApiKey = apiKey;
            instance.Slow = slow;
            OnCreated(EventArgs.Empty);
            return instance;
        }

        /// <summary>
        /// Connects to Teamwork authentication server with given APiKey to get BaseUrl.
        /// Returns a value indicating whether BaseUrls successfully retrieved.
        /// Immediately returns true if BaseUrl was already specified.
        /// </summary>
        public bool TryLogin()
        {
            if (BaseUrl == null)
            {
                using (var wc = new WebClient())
                {
                    wc.Credentials = new NetworkCredential(ApiKey, "X");
                    string xml;

                    xml = wc.DownloadString("http://authenticate.teamworkpm.net/authenticate.xml");

                    var doc = XDocument.Parse(xml);
                    var url = doc.Descendants("url").FirstOrDefault();
                    if (url != null)
                        BaseUrl = url.Value;
                    return BaseUrl != null;
                }
            }
            return true;
        }

        private static void OnCreated(EventArgs e)
        {
            var h = Created;
            if (h != null)
                h.Invoke(typeof(TeamWorkClient), e);
        }
    }
}